package com.unares.operation;

import com.unares.CUnares;

public class CSin extends CUnares {
    double x;

    public CSin(double x){
        super(x);
        this.x = x;
    }

    @Override
    public double calc() {
        return Math.sin(x);
    }

    @Override
    public String expressionToString() {
        return "sin(" + x + ")";
    }

    @Override
    public String resultToString(){
        return Double.toString(calc());
    }
}
