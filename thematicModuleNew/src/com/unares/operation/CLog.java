package com.unares.operation;

import com.unares.CUnares;

public class CLog extends CUnares {
    double x;

    public CLog(double x){
        super(x);
        this.x = x;
    }

    @Override
    public double calc() {
        return Math.log(x);
    }

    @Override
    public String expressionToString() {
        return "ln(" + x + ")";
    }

    @Override
    public String resultToString(){
        return Double.toString(calc());
    }
}
