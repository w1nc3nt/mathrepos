package com.unares.operation;

import com.unares.CUnares;

public class CExp extends CUnares {
    double x;

    public CExp(double x){
        super(x);
        this.x = x;
    }

    @Override
    public double calc() {
        return Math.exp(x);
    }

    @Override
    public String expressionToString() {
        return "e^" + x;
    }

    @Override
    public String resultToString(){
        return Double.toString(calc());
    }
}
