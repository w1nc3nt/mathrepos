package com.unares.operation;

import com.unares.CUnares;

public class CTan extends CUnares {
    double x;

    public CTan(double x){
        super(x);
        this.x = x;
    }

    @Override
    public double calc() {
        return Math.tan(x);
    }

    @Override
    public String expressionToString() {
        return "tg(" + x + ")";
    }

    @Override
    public String resultToString(){
        return Double.toString(calc());
    }
}
