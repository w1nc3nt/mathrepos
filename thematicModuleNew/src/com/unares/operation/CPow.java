package com.unares.operation;

import com.unares.CUnares;

public class CPow extends CUnares {
    double x;
    double exponent;

    public CPow(double x, double exponent){
        super(x,exponent);
    }

    @Override
    public double calc() {
        return Math.pow(x,exponent);
    }

    @Override
    public String expressionToString() {
        return x + "^" + exponent;
    }

    @Override
    public String resultToString(){
        return Double.toString(calc());
    }
}
