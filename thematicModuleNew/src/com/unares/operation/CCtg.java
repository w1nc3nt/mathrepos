package com.unares.operation;

import com.unares.CUnares;

public class CCtg extends CUnares {
    double x;

    public CCtg(double x){
        super(x);
        this.x = x;
    }

    @Override
    public double calc() {
        return 1.0 / Math.tan(x);
    }

    @Override
    public String expressionToString() {
        return "ctg(" + x + ")";
    }

    @Override
    public String resultToString(){
        return Double.toString(calc());
    }
}
