package com.unares.operation;

import com.unares.CUnares;

public class CCos extends CUnares {
    double x;

    public CCos(double x){
        super(x);
        this.x = x;
    }

    @Override
    public double calc() {
        return Math.cos(x);
    }


    @Override
    public String expressionToString() {
        return "cos(" + x + ")";
    }

    @Override
    public String resultToString(){
        return Double.toString(calc());
    }
}
