package com.unares;

import com.CFunction;

public abstract class CUnares extends CFunction {
    public double x;
    public double exponent;

    public CUnares(double x){
        this.x = x;
    }

    public CUnares(double x, double exponent){
        this.x = x;
        this.exponent = exponent;
    }

    public abstract double calc();
    public abstract String resultToString();
    public abstract String expressionToString();
}
