package com;

import com.binares.operation.CMult;
import com.binares.operation.CSum;
import com.unares.operation.CPow;

import java.util.List;

public class CPolinom extends CFunction {
    private List<Double> coefficients;
    private double x;

    public CPolinom(List<Double> coefficients, double x){
        this.coefficients = coefficients;
        this.x = x;
    }

    @Override
    public double calc(){
        double res = 0;
        for (int i = 0; i < coefficients.size(); i++) {
            res = new CSum(res, new CMult(coefficients.get(i), new CPow(x,coefficients.size() - 1 - i).calc()).calc()).calc();
        }
        return res;
    }

    @Override
    public String expressionToString(){
        return "Polinom = [" + coefficients + "]";
    }

    @Override
    public String resultToString(){
        return Double.toString(calc());
    }
}
