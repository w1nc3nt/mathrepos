package com.binares.operation;

import com.binares.CBinares;

public class CSum extends CBinares {
     double left;
     double right;

    public CSum(double left, double right) {
        super(left,right);
        this.left = left;
        this.right = right;
    }

    @Override
    public double calc() {
        return left + right;
    }

    @Override
    public String expressionToString() {
        return left + "+" + right;
    }

    @Override
    public String resultToString(){
        return Double.toString(calc());
    }
}
