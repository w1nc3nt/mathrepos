package com.binares;

import com.CFunction;

public abstract class CBinares extends CFunction {
    public double left;
    public double right;

    public CBinares(double left, double right){
        this.left = left;
        this.right = right;
    }


    public abstract double calc();
    public abstract String resultToString();
    public abstract String expressionToString();
}
