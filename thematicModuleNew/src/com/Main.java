package com;

import com.binares.operation.CDiv;
import com.binares.operation.CMult;
import com.binares.operation.CSub;
import com.binares.operation.CSum;
import com.data.CConst;
import com.unares.operation.*;

import java.util.*;

public class Main {
    protected static CFunction f1;
    protected static CFunction f2;
    protected static CConst cConstant;
    protected static double x = 6;
    protected static double result;
    public final static double cconst = 1;
    protected static String expression = "cos(sin(20))+7+x^(2)";

    public static void main(String[] args) {
        result = y(x,expression);
        cConstant = new CConst();
        cConstant.setConstant(cconst);
        System.out.println("y(x) = "+expression); //Выводим выражение
        System.out.println("y(x) = " +result);//Делаем рассчеты и выводим результат
        System.out.println("Constant = "+cConstant.getConstant());
        manualToXML("Expression","y(x) = "+expression,Double.toString(result)); //Конвертация выражения и результата выражения в XML
    }

    public static double y(double x,String expression){
        List<String> expressions = parse(expression);
        return calc(x,expressions);
    }

    public static void manualToXML(String name, String expression, String result){
        new XMLConvert() {
            @Override
            public String resultToString() { return result; }

            @Override
            public String expressionToString() {
                return expression;
            }

            @Override
            public void ToXML(String filename){
                super.ToXML(filename);
            }

        }.ToXML(name);
    }

    private static String operators = "+-*/";
    private static String delimiters = "() " + operators;
    public static boolean flag = true;

    private static boolean isDelimiter(String token) {
        if (token.length() != 1) return false;
        for (int i = 0; i < delimiters.length(); i++) {
            if (token.charAt(0) == delimiters.charAt(i)) return true;
        }
        return false;
    }

    private static boolean isOperator(String token) {
        if (token.equals("u-")) return true;
        for (int i = 0; i < operators.length(); i++) {
            if (token.charAt(0) == operators.charAt(i)) return true;
        }
        return false;
    }

    private static boolean isFunction(String token) {
        if (token.equals("ctg") || token.equals("x^") || token.equals("cos") || token.equals("sin") || token.equals("ln") ||
                token.equals("e^x") || token.equals("tg")) return true;
        return false;
    }

    private static int priority(String token) {
        if (token.equals("(")) return 1;
        if (token.equals("+") || token.equals("-")) return 2;
        if (token.equals("*") || token.equals("/")) return 3;
        if (token.equals("^")) return 3;
        return 4;
    }

    public static List<String> parse(String infix) {
        List<String> postfix = new ArrayList<String>();
        Deque<String> stack = new ArrayDeque<String>();
        StringTokenizer tokenizer = new StringTokenizer(infix, delimiters, true);
        String prev = "";
        String curr = "";
        while (tokenizer.hasMoreTokens()) {
            curr = tokenizer.nextToken();
            if (!tokenizer.hasMoreTokens() && isOperator(curr)) {
                System.out.println("Некорректное выражение.");
                flag = false;
                return postfix;
            }
            if (curr.equals(" ")) continue;
            if (isFunction(curr)) stack.push(curr);
            else if (isDelimiter(curr)) {
                if (curr.equals("(")) stack.push(curr);
                else if (curr.equals(")")) {
                    while (!stack.peek().equals("(")) {
                        postfix.add(stack.pop());
                        if (stack.isEmpty()) {
                            System.out.println("Скобки не согласованы.");
                            flag = false;
                            return postfix;
                        }
                    }
                    stack.pop();
                    if (!stack.isEmpty() && isFunction(stack.peek())) {
                        postfix.add(stack.pop());
                    }
                }
                else {
                    if (curr.equals("-") && (prev.equals("") || (isDelimiter(prev)  && !prev.equals(")")))) {
                        // унарный минус
                        curr = "u-";
                    }
                    else {
                        while (!stack.isEmpty() && (priority(curr) <= priority(stack.peek()))) {
                            postfix.add(stack.pop());
                        }

                    }
                    stack.push(curr);
                }

            }

            else {
                postfix.add(curr);
            }
            prev = curr;
        }

        while (!stack.isEmpty()) {
            if (isOperator(stack.peek())) postfix.add(stack.pop());
            else {
                System.out.println("Скобки не согласованы.");
                flag = false;
                return postfix;
            }
        }
        return postfix;
    }

    public static Double calc(double x,List<String> postfix) {
        Deque<Double> stack = new ArrayDeque<Double>();
        for (String r : postfix) {
            switch (r) {
                case "tg":
                    f1 = new CTan(stack.pop());
                    stack.push(f1.calc());
                    break;
                case "ctg":
                    f1 = new CCtg(stack.pop());
                    stack.push(f1.calc());
                    break;
                case "sin":
                    f1 = new CSin(stack.pop());
                    stack.push(f1.calc());
                    break;
                case "cos":
                    f1 = new CCos(stack.pop());
                    stack.push(f1.calc());
                    break;
                case "ln":
                    f1 = new CLog(stack.pop());
                    stack.push(f1.calc());
                    break;
                case "x^":
                    f1 = new CPow(x, stack.pop());
                    stack.push(f1.calc());
                    break;
                case"e^x":
                    f1 = new CExp(x);
                    stack.push(f1.calc());
                    break;
                case "+":
                    f2 = new CSum(stack.pop(),stack.pop());
                    stack.push(f2.calc());
                    break;
                case "-":
                    Double b = stack.pop(); Double a = stack.pop();
                    f2 = new CSub(a,b);
                    stack.push(f2.calc());
                    break;
                case "*":
                    f2 = new CMult(stack.pop(),stack.pop());
                    stack.push(f2.calc());
                    break;
                case "/":
                    Double d = stack.pop(); Double c = stack.pop();
                    f2 = new CDiv(c,d);
                    stack.push(f2.calc());
                    break;
                case "u-":
                    stack.push(-stack.pop());
                    break;
                default:
                    stack.push(Double.valueOf(r));
                    break;
            }
          /*  if (r.equals("tg"))
            {
                stack.push(Math.sqrt(stack.pop()));
            }
            else if(r.equals("sin")){
                stack.push(Math.sin(stack.pop()));
            }
            else if (r.equals("cube")) {
                Double tmp = stack.pop();
                stack.push(tmp * tmp * tmp);
            } else if (r.equals("x^")){
                f1 = new CPow(x,stack.pop());
                stack.push(f1.calc());
            }
            else if (r.equals("+")) stack.push(stack.pop() + stack.pop());
            else if (r.equals("-")) {
                Double b = stack.pop(), a = stack.pop();
                stack.push(a - b);
            } else if (r.equals("*")) stack.push(stack.pop() * stack.pop());
            else if (r.equals("/")) {
                Double b = stack.pop(), a = stack.pop();
                stack.push(a / b);
            } else if (r.equals("u-")) stack.push(-stack.pop());
            else stack.push(Double.valueOf(r));*/
        }
        return stack.pop();
    }
}
