package com.company;

public class MathDivision extends XMLConvert {
    double operand1;
    String operand1Expression;
    double operand2;
    String operand2Expression;
    private static String xmlName = "MathDivision";

    /**
     * Реализация операции деления двух чисел
     * @param operand1 Первый аргумент
     * @param operand2 Второй аргумент
     */

    MathDivision(double operand1, double operand2){
        this.operand1 = operand1;
        this.operand2 = operand2;
    }

    MathDivision(String operand1Expression, String operand2Expression) {
        this.operand1Expression = operand1Expression;
        this.operand2Expression = operand2Expression;
    }

    public double getResult(){
        return operand1 / operand2;
    }

    public String expressionToString() {
        return operand1Expression + " / " + operand2Expression;
    }

        @Override
    public String resultToString(){ //Конвертація у рядок
        return Double.toString(getResult());
    }

    public void resultToXML(){
        super.resultToXML(xmlName);
    }
}
