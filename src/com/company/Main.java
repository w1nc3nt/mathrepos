package com.company;

import java.util.ArrayList;
import java.util.List;

//Calc
//To scring

public class Main {
	static double x1 = 67.6;
	static double x = 1;
	public static final double s = 4;
	//static double operand1 = 23.7;
	//static double operand2 = 78.1;
	static double y1;
	static double y2;
	static double y3;
	static double y4;
	//Function y1;

	static MathAddition mathAddition;
	static MathSubtract mathSubtract;
	static MathMultiply mathMultiply;
	static MathCos mathCos;
	static MathCoTan mathCoTan;
	static MathDivision mathDivision;
	static MathExponent mathExponent;
	static MathLog mathLog;
	static MathPow mathPow;
	static MathSin mathSin;
	static MathTan mathTan;
	static Polynomial polynomial;
	static List<Double> coefficients;

	public static void main(String[] args) {
		coefficients = new ArrayList<Double>();
		coefficients.add(1.0); coefficients.add(4.0); coefficients.add(2.0); coefficients.add(5.0);
		ReversePolishParser n = new ReversePolishParser();
		List<String> expression = n.parse("sin(x)");
		boolean flag = n.flag;
		if (flag) {
			for (String x : expression) System.out.print(x + " ");
			System.out.println();
			System.out.println(ReversePolishParser.calc(expression));
		}
//		initializeObjects(operand1,operand2,x1);
//		getResults();
		// getY();
//		toXML();
	 }

	public static void initializeObjects(double operand1, double operand2, double x){ //РРЅРёС†РёР°Р»РёР·РёСЂСѓРµРј РѕР±СЊРµРєС‚С‹.
		mathAddition = new MathAddition(operand1, operand2);
		mathSubtract = new MathSubtract(operand1, operand2);
		mathMultiply = new MathMultiply(operand1,operand2);
		mathDivision = new MathDivision(operand1, operand2);
		mathCos = new MathCos(x);
		mathSin = new MathSin(x);
		mathLog = new MathLog(x);
		mathTan = new MathTan(x);
		mathPow = new MathPow(x, 4);
		mathExponent = new MathExponent(x);
		mathCoTan = new MathCoTan(x);

	}
public static void getY(){
		y1 = new MathAddition(new MathMultiply(new MathPow(x,3).getResult(),2).getResult(), 1).getResult();
		String y1Expression = new MathAddition(new MathMultiply(new MathPow("x","3").expressionToString(),"2").expressionToString(), "1").expressionToString();
//		String y1Expression = new MathAddition(new MathMultiply(new MathPow(String.valueOf(x),"3").expressionToString(),"2").expressionToString(), "1").expressionToString();
		//2*x1^3 + 1
	    y2 = new MathSin(new MathCos(x).getResult()).getResult();
		String y2Expression = new MathSin(new MathCos("x").expressionToString()).expressionToString();
//		String y2Expression = new MathSin(new MathCos(String.valueOf(x)).expressionToString()).expressionToString();
        //y2 = Sin(Cos(x)));
		//-------- y2 = new MathDivision(x1,3).getResult() + new MathCos(x1).getResult() + new MathLog(x1).getResult();
		//-------- x1/3 + cos(x1) + log(x1)
		y3 = new MathAddition(new MathMultiply(x,2).getResult() + new MathMultiply(x,4).getResult(),4).getResult();
		String y3Expression = new MathAddition(new MathMultiply("x","2").expressionToString() + " + " + new MathMultiply("x","4").expressionToString(),"4").expressionToString();
//		String y3Expression = new MathAddition(new MathMultiply(String.valueOf(x),"2").expressionToString() + " + " + new MathMultiply(String.valueOf(x),"4").expressionToString(),"4").expressionToString();
		//x*2 + x*4 +4
	    y4 = new MathAddition(new MathCos(x).getResult(), 3).getResult();
//	    String y4Expression = new MathAddition(new MathCos(String.valueOf(x)).expressionToString(), "3").expressionToString();
	String y4Expression = new MathAddition(new MathCos("x").expressionToString(), "3").expressionToString();

	    System.out.println("y1 = " + y1Expression + " = " + y1);
		System.out.println("y2 = " + y2Expression + " = " + y2);
		System.out.println("y3 = " + y3Expression + " = " + y3);
	    System.out.println("y4 = " + y4Expression + " = " + y4);
	    manualConvertToXML("TemplateFile_y1", Double.toString(y1), y1Expression);
	    manualConvertToXML("TemplateFile_y2", Double.toString(y2), y2Expression);
	    manualConvertToXML("TemplateFile_y3", Double.toString(y3), y3Expression);
		manualConvertToXML("TemplateFile_y4", Double.toString(y4), y4Expression);
		polynomial = new Polynomial(coefficients,x);
	    double p = polynomial.getResult();
	    String pExpression  = new Polynomial(coefficients,"x").expressionToString();
		System.out.println("p = " + pExpression + " = " + p);
		manualConvertToXML("Polinom", Double.toString(p), pExpression);
	}

	/*public static void getResults(){ //РџРѕР»СѓС‡Р°РµРј СЂРµР·СѓР»СЊС‚Р°С‚ РѕРїРµСЂР°С†РёРё РІС‹С‡РёСЃР»РµРЅРёР№ Рё РІС‹РІРѕРґРёРј РІСЃС‘ РІ СЃС‚СЂРѕРєСѓ
		System.out.print(mathAddition.getResult() + "\n" + mathSubtract.getResult() + "\n" + mathMultiply.getResult() + "\n" + mathDivision.getResult() +
				"\n" + mathCos.getResult() + "\n" + mathSin.getResult() + "\n" + mathLog.getResult() + "\n" + mathTan.getResult() + "\n" + mathPow.getResult() + "\n"
				+ mathExponent.getResult() + "\n" + mathCoTan.getResult() + "\n" + polynomial.getResult() + "\n");
	}*/
	public static void toXML(){
		mathAddition.resultToXML();
		mathSubtract.resultToXML();
		mathMultiply.resultToXML();
		mathDivision.resultToXML();
		mathCos.resultToXML();
		mathSin.resultToXML();
		mathLog.resultToXML();
		mathTan.resultToXML();
		mathPow.resultToXML();
		mathExponent.resultToXML();
		mathCoTan.resultToXML();
		polynomial.resultToXML();
}

	public static void manualConvertToXML(String filename, String data, String expression){
		new XMLConvert() {
			@Override
			public String resultToString() {
				return data;
			}

			@Override
			public String expressionToString() {
				return expression;
			}

			@Override
			public void resultToXML(String filename) {
				super.resultToXML(filename);
			}

		}.resultToXML(filename);
	}

}
//