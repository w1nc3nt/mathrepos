package com.company;

public class MathExponent extends XMLConvert {
    double exp;
    String expExpression;
    private static String xmlName = "MathExponent";

    /**
     * Реализация функции exp(x) = e^x
     * @param exp Степень експоненты
     */

    MathExponent(double exp){
        this.exp = exp;
    }

    MathExponent(String expExpression) {
        this.expExpression = expExpression;
    }

    public String expressionToString(){
        return "e ^ " + expExpression;
    }


    public double getResult(){
        return Math.exp(exp);
    }

    @Override
    public String resultToString(){ //Конвертація у рядок
        return Double.toString(getResult());
    }

    public void resultToXML(){
        super.resultToXML(xmlName);
    }
}
