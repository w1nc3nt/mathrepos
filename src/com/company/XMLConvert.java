package com.company;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

/** Реализация конвертации результата операции в XML формат и запись его в файл
 * Путь сохранения: "C:\Math"
  */

public abstract class XMLConvert {
    private static String pathToFile = "C:\\Math";

    public abstract String resultToString();

    public abstract String expressionToString();


    public void resultToXML(String filename){
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        File directory = new File(pathToFile);
        try {
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(createDocument(resultToString(), expressionToString()));
            if(!directory.exists()){directory.mkdir();}
            StreamResult streamResult = new StreamResult(new File(pathToFile + "\\" +filename + ".xml"));
            try {
                transformer.transform(domSource, streamResult);
                System.out.println(filename + ".xml saved!");
            } catch (TransformerException e) {
                e.printStackTrace();
            }
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }

    }

    private Document createDocument(String valueStr, String expressionStr){
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        Document document = null;
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.newDocument();

            Element result = document.createElement("result");
            document.appendChild(result);

            result.setAttribute("id", "1");

            Element value = document.createElement("value");
            value.appendChild(document.createTextNode(valueStr));
            result.appendChild(value);

            Element expression = document.createElement("expression");
            expression.appendChild(document.createTextNode(expressionStr));
            result.appendChild(expression);

            return document;

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return document;
    }


}
