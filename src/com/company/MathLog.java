package com.company;

public class MathLog extends XMLConvert {
    double x;
    String xExpression;
    private static String xmlName = "MathLog";

    /**
     * Реализация функции lg(x)
     * @param x Угол
     */

    MathLog(double x){
        this.x = x;
    }

    MathLog(String xExpression) {
        this.xExpression = xExpression;
    }

    public double getResult(){
        return Math.log(x);
    }

    public String expressionToString(){
        return "log(" + xExpression +")";
    }

    @Override
    public String resultToString(){ //Конвертація у рядок
        return Double.toString(getResult());
    }

    public void resultToXML(){
        super.resultToXML(xmlName);
    }
}
