package com.company;

public class MathTan extends XMLConvert {
    double x;
    String xExpression;
    private static String xmlName = "MathTan";

    /**
     * Реализация функции tg(x)
     * @param x Угол
     */

    MathTan(double x){
        this.x = x;
    }

    MathTan(String xExpression) {
        this.xExpression = xExpression;
    }

    public double getResult(){
        return Math.tan(x);
    }

    public String expressionToString() {
        return "tan(" + xExpression + ")";
    }

    @Override
    public String resultToString(){ //Конвертація у рядок
        return Double.toString(getResult());
    }

    public void resultToXML(){
        super.resultToXML(xmlName);
    }
}
