package com.company;

import java.util.List;

public class Polynomial extends XMLConvert {
    private final List<Double> coefficients;
    private double x;
    private String xExpression;
    private static String xmlName = "Polynomical";


    Polynomial(List<Double> coefficients, double x){

        this.coefficients = coefficients;
        this.x = x;
    }

    Polynomial(List<Double> coefficients, String xExpression){

        this.coefficients = coefficients;
        this.xExpression = xExpression;
    }

    @Override
    public String resultToString(){ //Конвертація у рядок
        return Double.toString(getResult());
    }

    public double getResult(){
        //1.0*x^2 + 4.0*x + 2.0
        double res = 0;
        for (int i = 0; i < coefficients.size(); i++) {
            res = new MathAddition(res, new MathMultiply(coefficients.get(i), new MathPow(x,coefficients.size() - 1 - i).getResult()).getResult()).getResult();
        }
        return res;
    }

    public String expressionToString() {
        String expression = "";
        for (int i = 0; i < coefficients.size(); i++) {
            expression = new MathAddition(expression, new MathMultiply(String.valueOf(coefficients.get(i)), new MathPow(xExpression,String.valueOf(coefficients.size() - 1 - i)).expressionToString()).expressionToString()).expressionToString();
        }
        return expression;
    }


    public void resultToXML(){
        super.resultToXML(xmlName);
    }
}
