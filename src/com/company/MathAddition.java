package com.company;

public class MathAddition extends XMLConvert {
    double operand1;
    String operand1Expression;

    double operand2;
    String operand2Expression;
    private static String xmlName = "MathAddition";


    MathAddition(double operand1, double operand2){
        this.operand1 = operand1;
        this.operand2 = operand2;
    }

    MathAddition(String operand1Expression, String operand2Expression){
        this.operand1Expression = operand1Expression;
        this.operand2Expression = operand2Expression;
    }

    public double getResult(){
        return operand1 + operand2;
    }

    @Override
    public String resultToString(){ //Конвертація у рядок
        return Double.toString(getResult());
    }

    //---------------------------------
    public String expressionToString(){
        return operand1Expression + " + " + operand2Expression;
    }

    public void resultToXML(){
        super.resultToXML(xmlName);
    }


}
