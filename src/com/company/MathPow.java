package com.company;

public class MathPow extends XMLConvert {
    double base;
    String baseExpression;

    double exponent;
    String exponentExpression;

    private static String xmlName = "MathPow";

    /**
     * Реализация производной показательной функции a^x
     * @param base Производная e
     * @param exponent Степень x
     */

    MathPow(double base, double exponent){
        this.base = base;
        this.exponent = exponent;
    }

    MathPow(String baseExpression, String exponentExpression) {
        this.baseExpression = baseExpression;
        this.exponentExpression = exponentExpression;
    }

    public double getResult(){
        return Math.pow(base,exponent);
    }

    @Override
    public String resultToString(){ //Конвертація у рядок
        return Double.toString(getResult());
    }

    //---------------------------------
    public String expressionToString(){
        return baseExpression + "^" + exponentExpression;
    }


    public void resultToXML(){
        super.resultToXML(xmlName);
    }
}
