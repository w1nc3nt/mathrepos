package com.company;

public class MathSin extends XMLConvert {
    double x;
    String xExpression;
    private static String xmlName = "MathSin";

    /**
     * Реализация функции sin(x)
     * @param x Угол
     */

    MathSin(double x){
        this.x = x;
    }

    MathSin(String xExpression) {
        this.xExpression = xExpression;
    }

    public double getResult(){
        return Math.sin(x);
    }

    @Override
    public String resultToString(){ //Конвертація у рядок
        return Double.toString(getResult());
    }

    public String expressionToString(){
        return "sin("+xExpression+")";
    }

    public void resultToXML(){
        super.resultToXML(xmlName);
    }
}
