package com.company;

public class MathCos extends XMLConvert {
    double x;
    String xExpression;
    private static String xmlName = "MathCos";


    MathCos(double x){
        this.x = x;
    }

    MathCos(String xExpression) {
        this.xExpression = xExpression;
    }

    public double getResult(){
        return Math.cos(x);
    }

    @Override
    public String resultToString(){ //Конвертація у рядок
        return Double.toString(getResult());
    }

    public void resultToXML(){
        super.resultToXML(xmlName);
    }

    public String expressionToString(){
        return "cos("+xExpression+")";
    }
}
