package com.company;


public class MathCoTan extends XMLConvert {
    double x;
    String xExpression;
    private static String xmlName = "MathCoTan";

    /**
     * Реализация функции ctg(x)
     * @param x Угол
     */

    MathCoTan(double x){
        this.x = x;
    }

    MathCoTan(String xExpression) {
        this.xExpression = xExpression;
    }

    public double getResult(){
        return 1.0 / Math.tan(x);
    }

    public String expressionToString() {
        return "ctag(" + xExpression + ")";
    }

        @Override
    public String resultToString(){ //Конвертація у рядок
        return Double.toString(getResult());
    }

    public void resultToXML(){
        super.resultToXML(xmlName);
    }
}
